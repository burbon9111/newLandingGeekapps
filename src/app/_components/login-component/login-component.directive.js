(function(){
    'use strict';

    angular
      .module('app.components.login-component')
      .directive('loginComponent', function(){
          return {
              restrict: 'AE',
              scope: true,
              controller: 'LoginController',
              controllerAs: 'login',
              templateUrl: 'app/_components/login-component/login-component.template.html',
              link: function(){

              }
          }
      })

      .directive('passwordConfirm', ['$parse', function ($parse) {
         return {
            restrict: 'A',
            scope: {
              matchTarget: '=',
            },
            require: 'ngModel',
            link: function link(scope, elem, attrs, ctrl) {
              var validator = function (value) {
                ctrl.$setValidity('match', value === scope.matchTarget);
                return value;
              }

              ctrl.$parsers.unshift(validator);
              ctrl.$formatters.push(validator);

              scope.$watch('matchTarget', function(newval, oldval) {
                validator(ctrl.$viewValue);
              });

            }
          };
        }])
})();
