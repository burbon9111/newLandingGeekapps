(function () {
    'use strict';

    angular
        .module('app.components.login-component')
        .controller('LoginController', ['$scope', '$translate', '$translation', 'LoginService', 'Theme',
         function($scope, $translate, $translation, LoginService,  Theme){
            let vm = this;
            vm.isValid = false;

            vm.user = {
               email: '',
               password: '',
               confirmPass: '',
               phone: '',
               privacy: ''
            };
            
            vm.login = function () {
                if ($scope.loginForm.$invalid) {
                    //NotificationsService.informationNotification('ERROR_USERNAME_OR_PASSWORD_IS_INVALID');
                    return $q.reject();
                }
                return LoginService.login(vm.user.email, vm.user.password)
                    .then(function (data) {
                        //AuthService.setAuthorizedData(data.access_token, data.id, vm.user.remember);
                        //$localStorage.user = data;
                        ////$rootScope.email = '';
                        //$state.go('app.feed');
                        console.log(data)
                        return $q.resolve();
                    })
                    .catch(function (error) {
                        //NotificationsService.informationNotification(error || 'AN_INTERNAL_SERVER_ERROR');
                        return $q.reject();
                    });
            };

        }]);
}());
