(function() {
    'use strict';

    /**
     *
     * Inject all components
     *
     * */

    angular.module('app.components', ['app.components.login-component']);
})();
