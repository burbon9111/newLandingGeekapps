(function(){
    'use strict';

    angular
        .module('app.landing')

        .controller('LandingController', ['$rootScope','$scope', '$translate', '$translation', 'Theme',
         function($rootScope, $scope, $translate, $translation, Theme){
            let vm = this;
            let swiper = new Swiper('.swiper-container', {
              pagination: '.swiper-pagination',
              direction: 'vertical',
              slidesPerView: 1,
              paginationClickable: true,
              spaceBetween: 30,
              mousewheelControl: true,
              touchEventsTarget: 'body',
              mousewheelEventsTarged: 'body',
              resizeReInit:true,
            });


            $rootScope.theme = Theme.Colors[0];

            swiper.on('onSlideChangeEnd', function() {
              if (swiper.slides.length) {
                $rootScope.theme = Theme.Colors[swiper.activeIndex];
                $scope.$apply();
              }
            });

        }]);
})();
