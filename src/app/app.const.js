(function () {
    'use strict';

    angular
      .module('app')

      .constant('Theme', {
        Colors: {
          0: '#45b6ee',
          1: 'blue',
          2: 'green',
          3: 'grey',
          4: 'red'
        }
      })

      .constant('RtlLangs', {
        0: 'heb',
        1: 'chi'
      })

      .constant('server', {
         url: 'http://apps.artygeek.net',
      })

      .constant('defaultLang', 'en');

})();
