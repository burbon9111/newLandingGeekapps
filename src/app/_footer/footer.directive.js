(function(){
    'use strict';

    angular
        .module('app.footer')
        .directive('footerComponent', function(){
            return {
                restrict: 'AE',
                scope: true,
                controller: 'FooterController',
                controllerAs: 'footer',
                templateUrl: 'app/_footer/footer.template.html',
                link: function(){

                }
            }
        })

})();
