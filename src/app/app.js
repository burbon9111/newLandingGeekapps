(function() {
    'use strict';

    var appDependencies = [
        'ui.router',
        'ngCookies',
        'LocalStorageModule',
        'ksSwiper',
        'app.auth',
        'app.login',
        'app.translations',
        'app.layout',
        'app.footer',
        'app.header',
        'app.components',
        'app.landing'
    ];
    angular.module('app', appDependencies);
})();
