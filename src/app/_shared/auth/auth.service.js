(function () {
    'use strict';

    angular.module('app.auth')
        .service('AuthService', authService);
    function authService($localStorage, $sessionStorage) {
        var exports = {
            setAuthorizedData: function (token, id, remember) {
                if (remember) {
                    $localStorage.authData = {
                        token: token,
                        appId: id
                    };
                } else {
                    $sessionStorage.authData = {
                        token: token,
                        appId: id
                    };
                }
            },
            setSuperAdminAuthorizedData: function (token, id) {
                $sessionStorage.superAdminData = {
                    token: token,
                    appId: id
                };
            },
            isAuthorized: function () {
                return exports.getAuthorizedData();
            },

            isSuperAdmin: function () {
                return $sessionStorage.superAdminData || false;
            },

            getAuthorizedData: function () {
                if ($sessionStorage.superAdminData) {
                    return $sessionStorage.superAdminData;
                }
                return $localStorage.authData || $sessionStorage.authData;
            },

            removeAuthorizedData: function () {
                delete $localStorage.authData;
                delete $sessionStorage.authData;
                delete $sessionStorage.superAdminData;
            }
        };
        return exports;
    }

    angular.module('app.auth')
        .service('userAuthenticationInterceptor', userAuthenticationInterceptor);
    function userAuthenticationInterceptor($q, $location, $injector) {
        return {
            responseError: function (response) {
                var state = $injector.get('$state');
                var $rootScope = $injector.get('$rootScope');
                if (response.status === 401) {
                    state.go('login');
                }
                if (response.status === 400) {
                    if (response.data.message &&
                        (response.data.message === 'ERROR_TRIAL_PERIOD_IS_EXPIRED' ||
                        response.data.message === 'ERROR_APPLICATION_IS_STOPPED')) {
                        $rootScope.trialExpired = true;
                        state.go('pricing');
                    }
                    if (response.data.message &&
                        response.data.message === 'ERROR_LOGIN_IN_TRACKER_IS_REQUIRED') {
                        state.go('geekTracker');
                    }
                }
                return $q.reject(response);
            },

            request: function (config) {
                var auth = $injector.get('AuthService');
                var authData = auth.getAuthorizedData();
                config.headers = config.headers || {};
                if (authData && !config.noAuth) {
                    config.headers.Authorization = 'Bearer ' + authData.token;
                    if (authData.appId) {
                        config.headers.appId = authData.appId;
                    }
                }
                return config;
            }
        };
    }
}());
