(function (){
    'use strict';

    angular.module('app.login')
        .service('LoginService', ['$http', 'server', '$q',
            function($http, server, $q){
                var exports = {
                    login: function(userName, password){
                        return $http({
                            method: "POST",
                            //url: 'server.url+'token '',
                            data: 'role=0&grant_type=password&username='+userName+'&password='+password
                        })
                            .then(function(response){
                                return response.data;
                            })
                            .catch(function(response){
                                var error = response ? response.error : '';
                                return $q.reject(error);
                            });
                    }
                };
                return exports;
            }
        ])
})();
