(function(){
    'use strict';

    angular
        .module('app')
        .directive('dropdown', function () {
           return {
              restrict: 'A',

              link: function link(scope, elem, attrs, ctrl) {
                elem.bind('click', function() {
                  elem.toggleClass('open');
                });
              }
            };
          })

})();
