(function(){
  angular
   .module('app')

   .factory('$translation', ['$rootScope', '$translate', '$localstorage', 'RtlLangs',
    function($rootScope, $translate, $localstorage, RtlLangs) {
     return {
       changeLanguage: function(key) {
         $localstorage.set('lang', key);
       },

       checkIsRTL: function(key) {
         for (let i in RtlLangs){
           if(key === RtlLangs[i]) {
             $rootScope.isRTL = true;
             return;
           }
           else {
             $rootScope.isRTL = false;
           }
         }
       },

       validateLang: function(lang, defaultLang) {
         if(!lang || lang === undefined) {
           $translate.use(defaultLang);
         }
         else {
           $translate.use(lang);
         }
       }
     }
   }])
})();
