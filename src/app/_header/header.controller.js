(function(){
    'use strict';

    angular
        .module('app.header')
        .controller('HeaderController', ['$rootScope', '$scope', '$translate', '$translation', function($rootScope, $scope, $translate, $translation){
            let vm = this;

            vm.changeLanguage = function(key) {
              $translate.use(key);
              $translation.changeLanguage(key);
              $translation.checkIsRTL(key);
            }
        }]);
})();
