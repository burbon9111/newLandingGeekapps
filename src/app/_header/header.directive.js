(function(){
    'use strict';

    angular
        .module('app.header')
        .directive('headerComponent', function(){
            return {
                restrict: 'AE',
                scope: true,
                controller: 'HeaderController',
                controllerAs: 'header',
                templateUrl: 'app/_header/header.template.html',
                link: function(){

                }
            }
        })

})();
