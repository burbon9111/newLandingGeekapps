(function () {
    'use strict';

    angular
      .module('app')

      .run(['$translation', '$localstorage', 'defaultLang', function ($translation, $localstorage, defaultLang) {
        let currentLang = $localstorage.get('lang');
        let defaulLang = defaultLang;

        $translation.validateLang(currentLang, defaultLang);
        $translation.checkIsRTL(currentLang);
      }])

})();
